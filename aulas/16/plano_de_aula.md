### Plano de Aula: Normalização e Desnormalização em Bancos de Dados SQL Server

**Objetivo da Aula**: Compreender os conceitos de normalização, formas normais, dependência funcional, anomalias e desnormalização, e aplicar esses conceitos na prática usando SQL Server.

**Duração da Aula**: 2 horas

---

#### 1. Introdução à Normalização (30 minutos)

- **Definição**: Explicar o que é normalização e por que é importante para o design de bancos de dados.
- **Exemplo Prático**: Demonstrar como a normalização pode ser aplicada em um cenário de banco de dados real.

---

#### 2. Formas Normais (30 minutos)

- **Formas Normais**: Explicar as diferentes formas normais (1NF, 2NF, 3NF, BCNF) e quando aplicá-las.
- **Exemplo Prático**: Fornecer exemplos de como aplicar cada forma normal em um banco de dados.

---

#### 3. Dependência Funcional (30 minutos)

- **Dependência Funcional**: Explicar o conceito de dependência funcional e como ela se relaciona com a normalização.
- **Exemplo Prático**: Mostrar exemplos de dependência funcional e como ela pode ser usada para normalizar um banco de dados.

---

#### 4. Anomalias (30 minutos)

- **Anomalias**: Explicar os diferentes tipos de anomalias (inserção, atualização e exclusão) e como elas podem ser evitadas através da normalização.
- **Exemplo Prático**: Demonstrar exemplos de anomalias e como a normalização pode ser usada para resolvê-las.

---

#### 5. Desnormalização (30 minutos)

- **Desnormalização**: Explicar o que é desnormalização e quando ela pode ser usada para melhorar o desempenho do banco de dados.
- **Exemplo Prático**: Fornecer exemplos de como desnormalizar um banco de dados para otimização de desempenho.

---

#### 6. Exemplos e Exercícios (30 minutes)

- **Exemplos de Aplicação**: Apresentar exemplos de como os conceitos de normalização e desnormalização são aplicados em cenários do mundo real.
- **Exercícios de Prática**: Fornecer exercícios que exijam a normalização e desnormalização de tabelas em SQL Server.

---

#### 7. Revisão e Discussão (15 minutos)

- **Revisão dos Conceitos**: Revisar os principais conceitos discutidos na aula.
- **Discussão em Grupo**: Fomentar uma discussão em grupo sobre os desafios e soluções encontrados durante a normalização e desnormalização de tabelas.

---

#### Recursos Adicionais

- **Material de Apoio**: Fornecer slides, notas de aula e links para recursos adicionais sobre cada conceito e subconjunto do SQL.
- **Software de Demonstração**: Utilizar um software de demonstração para mostrar a normalização e desnormalização de tabelas em SQL Server.

---

#### Avaliação

- **Quiz**: Após a aula, distribuir um quiz para avaliar a compreensão dos alunos sobre os conceitos discutidos e o uso da normalização e desnormalização em SQL Server.
