# Aula 06

![Untitled](https://media.licdn.com/dms/image/D4D12AQFw7tNg-H4qmg/article-cover_image-shrink_600_2000/0/1686396790241?e=2147483647&v=beta&t=fNRYoTURNdQrCcCB4ZatgYkLyHZxpnpDeQ4mX5rJOu0)

💡 Conceito de aberto e fechado, princípio de substituição de Liskov, princípio de inversão de dependência e injeção de dependência. Exemplos e exercícios.

## Sumário

- [x]  O - Princípio do Aberto/Fechado (OCP - Open/Closed Principle)
- [x]  L - Princípio da Substituição de Liskov (LSP - Liskov Substitution Principle)
- [x]  I - Princípio da Segregação de Interfaces (ISP - Interface Segregation Principle)
- [x]  D - Princípio da Inversão de Dependência (DIP - Dependency Inversion Principle)
- [x]  Exercícios

## Conteúdo

### Matéria

#### O - Princípio do Aberto/Fechado (OCP - Open/Closed Principle)

O Princípio Aberto/Fechado (OCP) é um dos cinco princípios SOLID da programação orientada a objetos e é fundamental para escrever código robusto, flexível e de fácil manutenção. Em essência, o OCP estabelece que as entidades de software devem ser abertas para extensão, mas fechadas para modificação. Isso significa que você pode estender o comportamento de uma classe sem precisar alterar seu código-fonte original.

Vou explicar o conceito com mais detalhes e fornecer exemplos em C# para ilustrar como aplicar o OCP:

##### Conceito do OCP:

- **Aberto para Extensão**: Isso significa que você pode estender o comportamento de uma classe sem precisar modificar seu código-fonte. Isso é alcançado por meio de herança, interfaces ou composição.

- **Fechado para Modificação**: Uma vez que uma classe está implementada e testada, seu código não deve ser alterado, a menos que haja um erro que precise ser corrigido. Alterações na classe original podem introduzir bugs ou causar efeitos colaterais indesejados em outras partes do sistema.

##### Exemplo em C#:

Vamos considerar um cenário em que temos uma classe `Shape` que representa várias formas geométricas e precisamos calcular a área de cada uma delas. No entanto, queremos que nossa implementação seja aberta para extensão, de modo que possamos adicionar novas formas geométricas sem modificar o código existente.

```csharp
// Interface para representar qualquer forma geométrica
public interface IShape
{
    double Area();
}

// Implementação da classe para representar um retângulo
public class Rectangle : IShape
{
    public double Width { get; set; }
    public double Height { get; set; }

    public double Area()
    {
        return Width * Height;
    }
}

// Implementação da classe para representar um círculo
public class Circle : IShape
{
    public double Radius { get; set; }

    public double Area()
    {
        return Math.PI * Radius * Radius;
    }
}

// Classe que calcula a soma das áreas de várias formas geométricas
public class AreaCalculator
{
    public double TotalArea(IShape[] shapes)
    {
        double total = 0;
        foreach (var shape in shapes)
        {
            total += shape.Area();
        }
        return total;
    }
}
```

Neste exemplo, temos uma interface `IShape` que define um método `Area()` para calcular a área de qualquer forma geométrica. As classes `Rectangle` e `Circle` implementam essa interface e fornecem suas próprias implementações do método `Area()`. A classe `AreaCalculator` usa a interface `IShape` para calcular a soma das áreas de várias formas geométricas, sem precisar conhecer os detalhes de implementação de cada forma específica.

Se quisermos adicionar uma nova forma geométrica, como um triângulo, podemos simplesmente criar uma nova classe que implementa a interface `IShape` e fornece sua própria implementação do método `Area()`, sem precisar modificar o código existente. Isso exemplifica como o código está aberto para extensão, mas fechado para modificação, seguindo o princípio OCP.

#### L - Princípio da Substituição de Liskov (LSP - Liskov Substitution Principle)

O Princípio da Substituição de Liskov (Liskov Substitution Principle - LSP), um conjunto de diretrizes de design de software que visam criar sistemas mais robustos e flexíveis. Ele estabelece uma regra importante para a herança de classes, garantindo que as classes derivadas possam ser substituídas por suas classes base sem causar efeitos colaterais indesejados.

##### Definição do Conceito
Em essência, o LSP afirma que objetos de um tipo base devem poder ser substituídos por objetos de um tipo derivado sem alterar a correção do programa. Isso significa que uma classe derivada deve poder substituir sua classe base em qualquer contexto sem modificar o comportamento esperado do programa. Em outras palavras, o cliente de uma classe deve ser capaz de usar uma instância de uma subclasse sem saber que tipo específico de classe está sendo usada.

##### Exemplo
Considere uma hierarquia de classes para representar formas geométricas em um sistema. Temos uma classe base `Shape` e duas classes derivadas `Rectangle` e `Square`. A aplicação do LSP garante que podemos substituir objetos das classes derivadas por objetos da classe base sem afetar o comportamento do programa.

```csharp
public class Shape
{
    public virtual double Area()
    {
        return 0;
    }
}

public class Rectangle : Shape
{
    public double Width { get; set; }
    public double Height { get; set; }

    public override double Area()
    {
        return Width * Height;
    }
}

public class Square : Shape
{
    public double SideLength { get; set; }

    public override double Area()
    {
        return SideLength * SideLength;
    }
}

public void PrintArea(Shape shape)
{
    Console.WriteLine($"Area: {shape.Area()}");
}

// Em algum lugar no código...
Rectangle rectangle = new Rectangle { Width = 5, Height = 4 };
Square square = new Square { SideLength = 5 };

PrintArea(rectangle); // Output: Area: 20
PrintArea(square);   // Output: Area: 25
```

### Exercícios
1. Refatore o código acima para introduzir uma classe `Circle` que estenda a classe `Shape` e calcule a área de um círculo.
2. Implemente um método para imprimir o tipo de forma (círculo, retângulo, quadrado) juntamente com a área. Certifique-se de aplicar o LSP ao adicionar esta nova funcionalidade.

#### I - Princípio da Segregação de Interfaces (ISP - Interface Segregation Principle):

O Princípio da Segregação de Interfaces (ISP), um conjunto de diretrizes de design de software para criar sistemas mais flexíveis, escaláveis e fáceis de manter. O ISP é especialmente importante no contexto de interfaces em linguagens orientadas a objetos, como csharp, C#, e outras.

##### Definição:
O Princípio da Segregação de Interfaces (ISP) afirma que "uma classe não deve ser forçada a implementar interfaces que ela não utiliza. Em vez disso, devem ser criadas interfaces mais específicas para os clientes." Em termos simples, isso significa que as interfaces devem ser refinadas e especializadas para atender às necessidades específicas das classes que as implementam, em vez de serem genéricas e abrangentes.

##### Exemplos:
Para entendermos melhor o ISP, vamos considerar um exemplo comum em desenvolvimento de software: uma interface para um dispositivo de armazenamento. Digamos que temos uma interface `StorageDevice` com métodos como `read()`, `write()`, `delete()`, e `format()`. Agora, suponha que temos duas classes que implementam essa interface: `HardDisk` e `USBFlashDrive`. No entanto, o `HardDisk` não suporta operações de formatação, enquanto o `USBFlashDrive` não suporta operações de deleção. Se seguirmos uma abordagem onde todas as classes devem implementar todos os métodos da interface, estaríamos violando o ISP. Em vez disso, podemos criar interfaces mais específicas, como `Readable`, `Writable`, `Deletable`, e `Formattable`, e fazer com que as classes implementem apenas as interfaces relevantes para elas.

```csharp
interface Readable {
    void read();
}

interface Writable {
    void write();
}

interface Deletable {
    void delete();
}

interface Formattable {
    void format();
}

class HardDisk implements Readable, Writable {
    public void read() {
        // Implementação específica para leitura de disco rígido
    }

    public void write() {
        // Implementação específica para escrita em disco rígido
    }
}

class USBFlashDrive implements Readable, Writable, Deletable {
    public void read() {
        // Implementação específica para leitura de unidade flash USB
    }

    public void write() {
        // Implementação específica para escrita em unidade flash USB
    }

    public void delete() {
        // Implementação específica para exclusão em unidade flash USB
    }
}
```

#### D - Princípio da Inversão de Dependência (DIP - Dependency Inversion Principle):

No desenvolvimento de software, a manutenção, escalabilidade e flexibilidade do código são aspectos fundamentais para garantir a qualidade e a longevidade de um sistema. Para alcançar esses objetivos, os desenvolvedores recorrem a princípios de design, como o Princípio da Inversão de Dependência (DIP), uma das cinco diretrizes do SOLID.

##### Definição de Conceito

O Princípio da Inversão de Dependência propõe uma abordagem para reduzir o acoplamento entre diferentes partes de um sistema. Ele estabelece que os módulos de alto nível não devem depender diretamente dos módulos de baixo nível, mas sim de abstrações. Além disso, as abstrações não devem depender de detalhes concretos, mas sim os detalhes devem depender das abstrações.

##### Exemplos

**Exemplo 1: Dependência Direta**
Imagine um sistema onde uma classe `UserService` depende diretamente de uma classe `DatabaseService`. Nesse cenário, qualquer mudança na implementação do `DatabaseService` pode exigir modificações na classe `UserService`, violando o princípio de DIP.

```csharp
class UserService {
    private DatabaseService database;

    public UserService() {
        this.database = new DatabaseService();
    }

    // Métodos da UserService que utilizam diretamente o DatabaseService
}
```

**Exemplo 2: Utilizando Abstrações**
Para aplicar o DIP, podemos introduzir uma interface `DatabaseInterface` que será implementada pelo `DatabaseService`, permitindo que `UserService` dependa apenas da abstração, não da implementação concreta.

```csharp
interface DatabaseInterface {
    // Métodos da interface
}

class DatabaseService implements DatabaseInterface {
    // Implementação dos métodos da interface
}

class UserService {
    private DatabaseInterface database;

    public UserService(DatabaseInterface database) {
        this.database = database;
    }

    // Métodos da UserService que utilizam a abstração DatabaseInterface
}
```

#### Fontes extras de estudo

[Padrões de design SOLID](https://learn.microsoft.com/pt-br/shows/Visual-Studio-Toolbox/SOLID-Design-Patterns)

#### Projeto

[Link do Projeto](https://gitlab.com/Delatorrea/dotnet-6-crud-api)

#### Exercícios

1. Pesquise e compartilhe um exemplo de violação do ISP em um projeto de código aberto. Discuta como essa violação pode impactar a manutenção e a extensibilidade do sistema.
2. Considere uma aplicação web onde um serviço de envio de e-mails (`EmailService`) depende diretamente de uma implementação concreta de um serviço de armazenamento de dados (`StorageService`). Refatore o código para aplicar o DIP, introduzindo uma abstração entre esses serviços.
3. Pesquise e discuta outras situações práticas onde a aplicação do Princípio da Inversão de Dependência pode trazer benefícios significativos para a manutenibilidade e a flexibilidade do código.