﻿
ITurma turma = new Turma("1A");
Aluno aluno1 = new Aluno("João", 15, turma);
Aluno aluno2 = new Aluno("Maria", 16, turma);
Aluno aluno3 = new Aluno("José", 17, turma);
Professor professor = new Professor("Carlos", 40);
professor.AdicionarTurma(turma);
turma.AdicionarAluno(aluno1);
turma.AdicionarAluno(aluno2);
turma.AdicionarAluno(aluno3);
aluno1.AdicionarAvaliacao(new Avaliacao(8));
aluno1.AdicionarAvaliacao(new Avaliacao(7));
aluno2.AdicionarAvaliacao(new Avaliacao(9));
aluno2.AdicionarAvaliacao(new Avaliacao(6));
aluno3.AdicionarAvaliacao(new Avaliacao(5));
aluno3.AdicionarAvaliacao(new Avaliacao(4));
Console.WriteLine($"Média geral: {turma.CalcularMediaGeral()}");
