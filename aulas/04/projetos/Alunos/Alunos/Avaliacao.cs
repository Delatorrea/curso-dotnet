public class Avaliacao 
{
    public decimal Nota { get; set; }

    public string Conceito
    {
        get
        {
            if (Nota < 3)
                return "E";
            else if (Nota < 5)
                return "D";
            else if (Nota < 7)
                return "C";
            else if (Nota < 9)
                return "B";
            else
                return "A";
        }
    }

    public string Situacao
    {
        get
        {
            if (Nota < 5)
                return "Reprovado";
            else
                return "Aprovado";
        }
    }

    public string Resultado
    {
        get
        {
            return $"Conceito: {Conceito} - Situação: {Situacao}";
        }
    }

    public Avaliacao(decimal nota)
    {
        Nota = nota;
    }
}