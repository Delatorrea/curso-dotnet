﻿using System;
using System.IO;
using System.Threading.Tasks;
using OfficeOpenXml;

public class ExcelGenerator
{
    public async Task CreateExcelWithRandomDataAsync(string filePath)
    {
        // Cria um novo pacote Excel
        using (var package = new ExcelPackage())
        {
            // Adiciona uma nova planilha ao pacote
            var worksheet = package.Workbook.Worksheets.Add("Random Data");

            // Define o título da planilha
            worksheet.Cells[1, 1].Value = "Random Data";

            // Preenche a planilha com dados aleatórios
            var random = new Random();
            for (int row = 2; row <= 100; row++)
            {
                for (int col = 1; col <= 5; col++)
                {
                    worksheet.Cells[row, col].Value = random.Next(100); // Gera um número aleatório entre 0 e 99
                }
            }

            // Salva o pacote em um arquivo
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await package.SaveAsync(stream);
            }
        }
    }
}
