﻿var linhas = File.ReadAllLines("./data/empresas.csv");
var nomes = linhas
    .Select(linha => linha.Split(';'))
    .Select(parte => parte[0] + " " + parte[1])
.Where(nome => nome.Contains("DELATORRE"));

foreach (var nome in nomes)
{
    Console.WriteLine(nome);
}
