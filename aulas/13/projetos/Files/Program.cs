﻿
var tasks = new List<Task>();
var taskScheduler = new LimitedConcurrencyLevelTaskScheduler(10); // Limita a execução de 10 tasks simultaneamente
var factory = new TaskFactory(taskScheduler);

while (true)
{
    var task = factory.StartNew(() => ProcessImage()); // Processa a imagem em paralelo
    tasks.Add(task);

    // Limita o número de tasks em execução para evitar sobrecarga
    if (tasks.Count >= 10)
    {
        Task.WaitAny(tasks.ToArray());
        tasks.RemoveAll(t => t.IsCompleted);
    }
}
}

    static void ProcessImage()
    {
        // Implementação do processamento da imagem
        // Por exemplo, detectar objetos de interesse
    }






public class LimitedConcurrencyLevelTaskScheduler : TaskScheduler
{
    private readonly int _maxDegreeOfParallelism;
    private int _currentCount = 0;

    public LimitedConcurrencyLevelTaskScheduler(int maxDegreeOfParallelism)
    {
        _maxDegreeOfParallelism = maxDegreeOfParallelism;
    }

    protected override IEnumerable<Task> GetScheduledTasks()
    {
        return null;
    }

    protected override void QueueTask(Task task)
    {
        lock (this)
        {
            if (_currentCount < _maxDegreeOfParallelism)
            {
                Interlocked.Increment(ref _currentCount);
                TryExecuteTask(task);
            }
            else
            {
                base.QueueTask(task);
            }
        }
    }

    protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
    {
        return false;
    }

    protected override void OnTaskCompleted(Task task)
    {
        Interlocked.Decrement(ref _currentCount);
    }
}