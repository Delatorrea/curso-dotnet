# Aula 12

![Untitled](https://ardounco.sirv.com/WP_content.bytehide.com/2023/05/async-await-csharp.png)

💡 Conceito de programação assíncrona, async, await, callbacks e promises.

## Sumário

- [ ]  Programação Assíncrona
- [ ]  Async
- [ ]  Await
- [ ]  Callbacks
- [ ]  Promises

## Conteúdo

### Programação Assíncrona

A programação assíncrona é um conceito que permite a execução de operações de forma não sequencial, evitando bloqueios na execução de um programa. Em programação síncrona, as operações são executadas uma após a outra, e a próxima operação só começa após a conclusão da anterior. Isso pode levar a atrasos ou "delays" na execução do programa, especialmente quando operações demoradas, como leitura e escrita de arquivos ou chamadas de recursos de terceiros, são envolvidas. Na programação assíncrona, essas operações demoradas são executadas em paralelo, permitindo que outras operações continuem sendo executadas sem esperar a conclusão dessas operações. Isso é alcançado através do uso de callbacks, promises e, mais recentemente, das palavras-chave `async` e `await` em linguagens como C#.

A programação assíncrona é especialmente útil em situações onde operações independentes podem ser executadas em paralelo, como leitura e escrita de arquivos, chamadas de recursos de terceiros e lógicas que podem ser separadas da execução da thread principal. Isso melhora a fluidez e a eficiência da aplicação, permitindo que o usuário continue interagindo com a aplicação enquanto operações demoradas estão sendo processadas em segundo plano.

Em termos práticos, a programação assíncrona permite que o código chame uma função e continue executando outras linhas enquanto espera a resposta da função. Isso é útil para operações que provavelmente levarão algum tempo para serem executadas, como requisições de dados para o servidor. A execução assíncrona é implementada de maneiras diferentes em linguagens de programação, mas geralmente envolve o uso de threads e loops de eventos.

### Programação Assíncrona no C#

A execução de operações assíncronas no C# é facilitada principalmente pelas palavras-chave async e await. Quando um método é marcado com async, ele pode conter uma ou mais expressões await, que indicam pontos de suspensão no método. O operador await informa ao compilador que o método assíncrono não pode continuar além desse ponto até que a operação assíncrona aguardada seja concluída. Durante esse tempo, o controle retorna para o chamador do método assíncrono, permitindo que outras operações sejam executadas sem bloquear o thread principal.

Métodos assíncronos normalmente têm um tipo de retorno de Task ou Task<TResult>, dependendo se eles retornam um valor ou não. A convenção é que o nome de um método assíncrono termine com um sufixo "Async". Mesmo sem expressões await, um método marcado como async é tratado como assíncrono pelo compilador, mas sem a capacidade de suspensão, ele é executado de forma síncrona. O uso de async e await simplifica significativamente a escrita de código assíncrono, permitindo que desenvolvedores escrevam código que parece síncrono, mas que é executado de forma assíncrona por baixo dos panos.

Para desenvolver uma aula sobre o conceito de programação assíncrona, `async`, `await`, callbacks e promises em C#, é importante começar explicando o que é programação assíncrona e por que ela é importante. A programação assíncrona permite que operações de I/O (como leitura e escrita de arquivos, ou solicitações de rede) sejam executadas sem bloquear o thread principal da aplicação, melhorando a capacidade de resposta e a eficiência do código.

### Conceitos Básicos

- **Callbacks**: São funções que são passadas como argumento para outra função e são executadas após a conclusão de uma operação assíncrona. No entanto, o uso excessivo de callbacks pode levar a um código difícil de ler e manter, conhecido como "callback hell".

- **Promises**: Uma maneira mais organizada de lidar com operações assíncronas. Uma promise representa uma operação que ainda não foi concluída, mas pode ser concluída no futuro. Promises permitem encadear operações assíncronas de forma mais limpa e legível.

- **Async/Await**: Introduzidos no C# 5.0, `async` e `await` são palavras-chave que permitem escrever código assíncrono de forma mais simples e direta. Uma função marcada com `async` retorna um `Task` ou `Task<T>`, e o `await` é usado para esperar a conclusão de uma operação assíncrona sem bloquear o thread.

### Exemplos

#### Callbacks

```csharp
public void LoadData(Action<string> callback)
{
    Task.Run(() =>
    {
        // Simula uma operação de I/O
        Thread.Sleep(1000);
        callback("Dados carregados");
    });
}

LoadData(data => Console.WriteLine(data));
```

#### Promises (Task e Task<T>)

```csharp
public Task<string> LoadDataAsync()
{
    return Task.Run(() =>
    {
        // Simula uma operação de I/O
        Thread.Sleep(1000);
        return "Dados carregados";
    });
}

LoadDataAsync().ContinueWith(task => Console.WriteLine(task.Result));
```

#### Async/Await

```csharp
public async Task LoadDataAsync()
{
    await Task.Run(() =>
    {
        // Simula uma operação de I/O
        Thread.Sleep(1000);
    });
    Console.WriteLine("Dados carregados");
}

await LoadDataAsync();
```

### Exercícios

1. **Refatoração de Callbacks para Async/Await**: Dado um exemplo de código que usa callbacks para carregar dados de uma API, refatore o código para usar `async` e `await`.

2. **Comparação de Performance**: Escreva um programa que compara o tempo de execução de uma operação de I/O usando callbacks, promises e async/await. Use um método de I/O que você está familiarizado, como a leitura de um arquivo grande ou uma solicitação de rede.

3. **Manipulação de Erros**: Modifique o exemplo anterior para incluir tratamento de erros usando `try/catch` em cada abordagem.

Ao final da aula, os alunos devem entender a diferença entre callbacks, promises e async/await, e ser capazes de escrever e modificar código assíncrono usando essas técnicas.